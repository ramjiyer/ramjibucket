import unittest
import webapp2
import shutil
import os
from pprint import pprint
from webtest import TestApp
from main import Ramjitracker

class Test5xxreturncodes(unittest.TestCase):

    def setUp(self):
        # Create a WSGI application.
        app = webapp2.WSGIApplication([
          (r'/urlinfo/1/(.*)', Test5xxreturncodes),
        ])
        self.testapp = TestApp('http://localhost:9080')
        shutil.copy2(os.getcwd()+'/main.py',os.getcwd()+'/tests/main.bak')
        shutil.copy2(os.getcwd()+'/tests/badmain.py',os.getcwd()+'/main.py')


    def test_not_error_500(self):
        response = self.testapp.get('/urlinfo/1/ramji.com',status=200)
        self.assertEqual(response.status_int, 200)


    def tearDown(self):
        # Create a WSGI application.
        shutil.copy2(os.getcwd()+'/tests/main.bak',os.getcwd()+'/main.py')
