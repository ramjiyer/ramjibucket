import webapp2

class BaseHandler(webapp2.RequestHandler):

    def handle_exception(self, exception, debug_mode):
        """ This method handles all the 5xx HTTP return codes """
        self.redirect('http://localhost:8080' + self.request.path + '?' + self.request.query_string)

class Ramjitracker(BaseHandler):

    def get(self, input_url):
        self.response.headers['Content-Type'] = 'text/html'
        slf.response.write(input_url)


class Mocktracker(BaseHandler):
    

    http_codes = ['501','502','503','504','505']

    def get(self, input_url):
        if input_url in self.http_codes:
            self.abort(int(input_url))

application = webapp2.WSGIApplication([
    (r'/urlinfo/1/(.*)', Ramjitracker),
    (r'/mockurlinfo/1/(.*)', Mocktracker),
], debug=False)

