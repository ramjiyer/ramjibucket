import unittest
import webapp2
import shutil
import os
from pprint import pprint
from webtest import TestApp
from main import Mocktracker

class TestMocktracker(unittest.TestCase):
    """Class to test the url lookup  webservice"""

    def setUp(self):
        # Create a WSGI application.
        app = webapp2.WSGIApplication([
         (r'/mockurlinfo/1/(.*)', Mocktracker)
        ],debug=False)
        self.testapp = TestApp('http://localhost:9080')


    def test501errorcode(self):
       """This test tests a simple request with a proper URl data"""

       response = self.testapp.get("/mockurlinfo/1/501",status=302)
       self.assertEqual(response.status_int, 302)
       self.assertEqual(response.content_type, 'text/html')
 

    def test502errorcode(self):
       """This test tests a simple request with a proper URl data"""

       response = self.testapp.get("/mockurlinfo/1/502",status=302)
       self.assertEqual(response.status_int, 302)
       self.assertEqual(response.content_type, 'text/html')

    def test503errorcode(self):
       """This test tests a simple request with a proper URl data"""

       response = self.testapp.get("/mockurlinfo/1/503",status=302)
       self.assertEqual(response.status_int, 302)
       self.assertEqual(response.content_type, 'text/html')

    def test504errorcode(self):
       """This test tests a simple request with a proper URl data"""

       response = self.testapp.get("/mockurlinfo/1/504",status=302)
       self.assertEqual(response.status_int, 302)
       self.assertEqual(response.content_type, 'text/html')

    def test505errorcode(self):
       """This test tests a simple request with a proper URl data"""

       response = self.testapp.get("/mockurlinfo/1/505",status=302)
       self.assertEqual(response.status_int, 302)
       self.assertEqual(response.content_type, 'text/html')


