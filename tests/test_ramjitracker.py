import unittest
import webapp2
import shutil
import os
from pprint import pprint
from webtest import TestApp
from main import Ramjitracker

class TestRamjitracker(unittest.TestCase):
    """Class to test the url lookup  webservice"""

    def setUp(self):
        # Create a WSGI application.
        app = webapp2.WSGIApplication([
          (r'/urlinfo/1/(.*)', Ramjitracker),
        ],debug=False)

        self.testapp = TestApp(app)

    def testgoodUrl(self):
       """This test tests a simple request with a proper URl data"""

       response = self.testapp.get("/urlinfo/1/www.mylibrary.com:80/classic/books?name=c_plus_plus")
       self.assertEqual(response.status_int, 200)
       self.assertEqual(response.content_type, 'text/html')


